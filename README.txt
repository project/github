#GitHub

Display ribbon on page that links to your GitHub-user

## Block placement

(See: Configuration > System > GitHub > Appearance)

1. If you're having problems displaying the ribbon on your site, or you want to
reposition the ribbon, edit 'Block placement'.

2. This option lets you change the region the ribbon is placed in.

3. As default the ribbon is placed in the debugging region 'Page top', and
this works with the following themes (if no custom
styling has overridden it):

* Bartik
* Fusion Starter
* Garland
* Seven
* Stark
* Zen
